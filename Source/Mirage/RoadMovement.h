// Copyright 2019 Necronomineko Studios. All Rights Reserved.

#pragma once

// Default includes
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

// Custom includes
#include "Gameframework/Actor.h"
#include "PlayerMovement.h"
#include "Containers/List.h"

// Generated default include - always last include
#include "RoadMovement.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MIRAGE_API URoadMovement : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	URoadMovement();
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	UPROPERTY(EditAnywhere)
	float MaxSpeed;
	UPROPERTY(EditAnywhere)
	float SpeedMult;

	UPROPERTY()
	AActor* Owner = nullptr;

	UPROPERTY()
	UPlayerMovement* PlayerMovement = nullptr;

	/** 
	 * Array of Road Tiles used to initialize the DoubleLinkedList RoadTiles, needs to be sorted Start to End.
	 * Do not use directly for any other purporse.
	 */
	UPROPERTY(EditAnywhere)
	TArray<AActor*> InitialRoadTilesArray;
	TDoubleLinkedList<AActor*> RoadTiles;

	UPROPERTY(EditAnywhere)
	AActor* Player = nullptr;

	UPROPERTY(EditAnywhere)
	AActor* RoadStart = nullptr;
	UPROPERTY(EditAnywhere)
	AActor* RoadEnd = nullptr;
	float RoadStartToEndDistance = 0.f;

	/** Initialize a DoubleLinkedList from an Array */
	void InitializeDoubleLinkedListFromArray(TDoubleLinkedList<AActor*> &DoubleLinkedList, const TArray<AActor*> &Array);

	/** Moves all the tiles an equal distance to Player movement in opposite direction */
	void MoveRoadTiles();
	/** Wrap any road tiles past RoadStart and RoadEnd to get a continious treadmill effect */
	void WrapRoadTiles(float Movement);

	/** Returns wether not RoadTile has passed the RoadStart */
	bool PassedRoadStart(AActor* RoadTile) const;
	/** Returns wether not RoadTile has passed the RoadEnd */
	bool PassedRoadEnd(AActor* RoadTile) const;

	/** Get the distance between RoadTile and RoadStart */
	float RoadStartDistance(AActor* RoadTile) const;
	/** Get the distance between RoadTile and RoadEnd */
	float RoadEndDistance(AActor* RoadTile) const;

	/** Teleport RoadTile to RoadStart and apply an offset*/
	void TeleportToRoadStart(AActor* RoadTile);
	/** Teleport RoadTile to RoadEnd and apply an offset */
	void TeleportToRoadEnd(AActor* RoadTile);

	/** Move the RoadTile Node to the Head of the list */
	void MoveRoadTileToHead(TDoubleLinkedList<AActor*>::TDoubleLinkedListNode* RoadTile);
	/** Move the RoadTile Node to the tail of the list */
	void MoveRoadTileToTail(TDoubleLinkedList<AActor*>::TDoubleLinkedListNode* RoadTile);

	/** Gets the players movement */
	float GetPlayerMovement() const;
};
