// Copyright 2019 Necronomineko Studios. All Rights Reserved.

#include "RoadMovement.h"
#include "Math/UnrealMathUtility.h"
#include "GameFramework/CharacterMovementComponent.h"

// Sets default values for this component's properties
URoadMovement::URoadMovement()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void URoadMovement::BeginPlay()
{
	Super::BeginPlay();

	Owner = GetOwner();
	if(InitialRoadTilesArray.Num() == 0)	UE_LOG(LogTemp, Error, TEXT("No Road Tiles found for actor: %s"), *Owner->GetName())
	else	InitializeDoubleLinkedListFromArray(RoadTiles, InitialRoadTilesArray);

	if(RoadStart == nullptr)	UE_LOG(LogTemp, Error, TEXT("RoadStart not assigned for actor: %s"), *Owner->GetName())
	if(RoadEnd == nullptr)		UE_LOG(LogTemp, Error, TEXT("RoadEnd not assigned for actor: %s"), *Owner->GetName())
	if(RoadStart != nullptr && RoadEnd != nullptr)	RoadStartToEndDistance = FMath::Abs(RoadStart->GetActorLocation().X - RoadEnd->GetActorLocation().X);

	if(Player != nullptr){
		PlayerMovement = Player->FindComponentByClass<UPlayerMovement>();
		
		if(PlayerMovement == nullptr)	UE_LOG(LogTemp, Error, TEXT("PlayerMovement not found for player: %s on actor: %s"), *Player->GetName(), *Owner->GetName())
		else {
			// Only tick Primary Tick after PlayerMovement's Primary Tick has ticked.
			PrimaryComponentTick.AddPrerequisite(PlayerMovement, PlayerMovement->PrimaryComponentTick);
			
			// Set Player to ignore collisions with all of the RoadTiles.
			for(auto RoadTile : RoadTiles)
				PlayerMovement->IgnoreCollisionsWithActor(RoadTile);
		}

	} else	UE_LOG(LogTemp, Error, TEXT("Player not assigned for actor: %s"), *Owner->GetName())
}


// Called every frame
void URoadMovement::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	float Movement = GetPlayerMovement();
	if(Movement != 0){
		MoveRoadTiles();
		WrapRoadTiles(Movement);
	}
}

void URoadMovement::InitializeDoubleLinkedListFromArray(TDoubleLinkedList<AActor*> &DoubleLinkedList, const TArray<AActor*> &Array){
	for(auto Element : Array)
		DoubleLinkedList.AddTail(Element);
}

void URoadMovement::MoveRoadTiles(){
	float Movement = GetPlayerMovement();
	FVector Location;

	for(auto RoadTile : RoadTiles){
		Location = RoadTile->GetActorLocation();
		Location.X += -(Movement > 0 ? FMath::Clamp(Movement, 0.f, MaxSpeed) : FMath::Clamp(Movement, -MaxSpeed, 0.f) * SpeedMult);
		RoadTile->SetActorLocation(Location);
	}
}

void URoadMovement::WrapRoadTiles(float Movement){
	if(Movement > 0)
	{
		// Keep moving the tail roadtile until there are no more road tiles past the roadend.
		while(PassedRoadEnd(RoadTiles.GetTail()->GetValue()))
		{
			TeleportToRoadStart(RoadTiles.GetTail()->GetValue());
			MoveRoadTileToHead(RoadTiles.GetTail());
		}
	}
	else if (Movement < 0)
	{
		// Keep moving the head roadtile until there are no more road tiles past the roadstart.
		while(PassedRoadStart(RoadTiles.GetHead()->GetValue()))
		{
			TeleportToRoadEnd(RoadTiles.GetHead()->GetValue());
			MoveRoadTileToTail(RoadTiles.GetHead());
		}
	}
}

bool URoadMovement::PassedRoadStart(AActor* RoadTile) const {
	if(RoadStart == nullptr || RoadTile == nullptr)	return false;

	return RoadTile->GetActorLocation().X > RoadStart->GetActorLocation().X;
}

bool URoadMovement::PassedRoadEnd(AActor* RoadTile) const {
	if(RoadEnd == nullptr || RoadTile == nullptr)	return false;

	return RoadTile->GetActorLocation().X < RoadEnd->GetActorLocation().X;
}

float URoadMovement::RoadStartDistance(AActor* RoadTile) const {
	if(RoadStart == nullptr)	return 0.f;

	return FMath::Abs(RoadStart->GetActorLocation().X - RoadTile->GetActorLocation().X);
}

float URoadMovement::RoadEndDistance(AActor* RoadTile) const {
	if(RoadEnd == nullptr)	return 0.f;

	return FMath::Abs(RoadEnd->GetActorLocation().X - RoadTile->GetActorLocation().X);
}

void URoadMovement::TeleportToRoadStart(AActor* RoadTile){
	if(RoadStart == nullptr || RoadEnd == nullptr || RoadTile == nullptr)	return;

	FVector Location;

	Location = RoadStart->GetActorLocation();
	Location.X -= FMath::Fmod(RoadEndDistance(RoadTile), RoadStartToEndDistance);

	RoadTile->SetActorLocation(Location, false, nullptr, ETeleportType::TeleportPhysics);
}

void URoadMovement::TeleportToRoadEnd(AActor* RoadTile){
	if(RoadStart == nullptr || RoadEnd == nullptr || RoadTile == nullptr)	return;

	FVector Location;

	Location = RoadEnd->GetActorLocation();
	Location.X += FMath::Fmod(RoadStartDistance(RoadTile), RoadStartToEndDistance); 
	
	RoadTile->SetActorLocation(Location, false, nullptr, ETeleportType::TeleportPhysics);
}

void URoadMovement::MoveRoadTileToHead(TDoubleLinkedList<AActor*>::TDoubleLinkedListNode* RoadTile)
{
	// Remove the RoadTile from the List but don't delete the node.
	RoadTiles.RemoveNode(RoadTile, false);
	// Set the RoadTile as head of the List
	RoadTiles.AddHead(RoadTile);
}
void URoadMovement::MoveRoadTileToTail(TDoubleLinkedList<AActor*>::TDoubleLinkedListNode* RoadTile)
{
	// Remove the RoadTile from the List but don't delete the node.
	RoadTiles.RemoveNode(RoadTile, false);
	// Set the RoadTile as Tail of the List
	RoadTiles.AddTail(RoadTile);
}

float URoadMovement::GetPlayerMovement() const {
	if(PlayerMovement == nullptr)	return 0.f;

	return PlayerMovement->GetMovement();
}