// Copyright 2019 Necronomineko Studios. All Rights Reserved.


#include "PlayerMovement.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Math/UnrealMathUtility.h"

// Sets default values for this component's properties
UPlayerMovement::UPlayerMovement()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	PostPhysicsTickFunction.TickGroup = TG_LastDemotable;
	PostPhysicsTickFunction.bStartWithTickEnabled = true;
	PostPhysicsTickFunction.bCanEverTick = true;

}


// Called when the game starts
void UPlayerMovement::BeginPlay()
{
	Super::BeginPlay();

	Owner = Cast<APawn>(GetOwner());
	if(Owner == nullptr){
		UE_LOG(LogTemp, Error, TEXT("Owner (APawn) not found for PlayerMovement!"))
		return;
	}

	StartLocation = Owner->GetActorLocation();
	Boundaries.X += StartLocation.X;
	Boundaries.Y += StartLocation.X;
	Boundaries.Z += StartLocation.Y;
	Boundaries.W += StartLocation.Y;

	UCharacterMovementComponent* CharacterMovement = Owner->FindComponentByClass<UCharacterMovementComponent>();
	if(CharacterMovement == nullptr)	UE_LOG(LogTemp, Error, TEXT("CharacterMovement not found on actor: %s"), *Owner->GetName())
	else	PrimaryComponentTick.AddPrerequisite(CharacterMovement, CharacterMovement->PostPhysicsTickFunction); //AddTickPrerequisiteComponent(CharacterMovement);
}

void FPostPhysicsTickFunction::ExecuteTick(float DeltaTime, enum ELevelTick TickType, ENamedThreads::Type CurrentThread, const FGraphEventRef& MyCompletionGraphEvent)
{
	ExecuteTickHelper(Target, Target->bTickInEditor, DeltaTime, TickType, [this, TickType](float DilatedTime)
	{
		Target->TickComponentPostPhysics(DilatedTime, TickType, this);
	});
}

FString FPostPhysicsTickFunction::DiagnosticMessage()
{
	return Target->GetFullName() + TEXT("[TickComponent]");
}

FName FPostPhysicsTickFunction::DiagnosticContext(bool bDetailed)
{
	if (bDetailed)
	{
		AActor* OwningActor = Target->GetOwner();
		FString OwnerClassName = OwningActor ? OwningActor->GetClass()->GetName() : TEXT("None");
		// Format is "ComponentClass/OwningActorClass/ComponentName"
		FString ContextString = FString::Printf(TEXT("%s/%s/%s"), *Target->GetClass()->GetName(), *OwnerClassName, *Target->GetName());
		return FName(*ContextString);
	}
	else
	{
		return Target->GetClass()->GetFName();
	}
}

void UPlayerMovement::RegisterComponentTickFunctions(bool bRegister)
{
	Super::RegisterComponentTickFunctions(bRegister);

	if(bRegister)
	{
		if(SetupActorComponentTickFunction(&PostPhysicsTickFunction))
		{
			PostPhysicsTickFunction.Target = this;
		}
	}
	else
	{
		if(PostPhysicsTickFunction.IsTickFunctionRegistered())
		{
			PostPhysicsTickFunction.UnRegisterTickFunction();
		}
	}
}


// Called every frame
void UPlayerMovement::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if(Owner == nullptr)	return;
	
	UpdateMovement();
}

void UPlayerMovement::TickComponentPostPhysics(float DeltaTime, ELevelTick TickType, FPostPhysicsTickFunction* ThisTickFunction)
{
	if(Owner == nullptr)	return;

	ResetLocation();
	LocationLastFrame = Owner->GetActorLocation();
}

void UPlayerMovement::UpdateMovement()
{
	if(Owner == nullptr)	return;

	if(ShouldRegisterMovement())	Movement = Owner->GetActorLocation().X - LocationLastFrame.X;
	else	 Movement = 0.f;
}

float UPlayerMovement::GetMovement() const
{
	return Movement;
}

FVector4 UPlayerMovement::GetBoundaries() const
{
	return Boundaries;
}

bool UPlayerMovement::ShouldRegisterMovement() const
{
	if(Owner == nullptr)	return false;

	FVector Location = Owner->GetActorLocation();
	if(Location.X < Boundaries.X || Location.X > Boundaries.Y)	return true;
	else 	return false;
}

void UPlayerMovement::ResetLocation()
{
	FVector Location = Owner->GetActorLocation();
	
	Location.X = FMath::Clamp(Location.X, Boundaries.X, Boundaries.Y);
	Location.Y = FMath::Clamp(Location.Y, Boundaries.Z, Boundaries.W);

	Owner->SetActorLocation(Location, false, nullptr, ETeleportType::TeleportPhysics);
}

void UPlayerMovement::IgnoreCollisionsWithActor(AActor* ActorToIgnore) const
{
	if(Owner == nullptr)	return;

	Owner->MoveIgnoreActorAdd(ActorToIgnore);
}

