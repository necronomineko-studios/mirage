// Copyright 2019 Necronomineko Studios. All Rights Reserved.

#pragma once

// Default Includes
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

// Custom Includes
#include "GameFramework/Actor.h"
#include "GameFramework/Pawn.h"
#include "Engine/EngineBaseTypes.h"

// Default Generated Include - Always Last
#include "PlayerMovement.generated.h"

USTRUCT()
struct FPostPhysicsTickFunction : public FTickFunction
{
	GENERATED_USTRUCT_BODY()

	/**  AActor  component that is the target of this tick **/
	class UPlayerMovement*	Target;

	/** 
		* Abstract function actually execute the tick. 
		* @param DeltaTime - frame time to advance, in seconds
		* @param TickType - kind of tick for this frame
		* @param CurrentThread - thread we are executing on, useful to pass along as new tasks are created
		* @param MyCompletionGraphEvent - completion event for this task. Useful for holding the completetion of this task until certain child tasks are complete.
	**/
	MIRAGE_API virtual void ExecuteTick(float DeltaTime, ELevelTick TickType, ENamedThreads::Type CurrentThread, const FGraphEventRef& MyCompletionGraphEvent) override;
	/** Abstract function to describe this tick. Used to print messages about illegal cycles in the dependency graph **/
	MIRAGE_API virtual FString DiagnosticMessage() override;
	MIRAGE_API virtual FName DiagnosticContext(bool bDetailed) override;

	/**
	 * Conditionally calls ExecuteTickFunc if bRegistered == true and a bunch of other criteria are met
	 * @param Target - the actor component we are ticking
	 * @param bTickInEditor - whether the target wants to tick in the editor
	 * @param DeltaTime - The time since the last tick.
	 * @param TickType - Type of tick that we are running
	 * @param ExecuteTickFunc - the lambda that ultimately calls tick on the actor component
	 */

	//NOTE: This already creates a UObject stat so don't double count in your own functions

	template <typename ExecuteTickLambda>
	static void ExecuteTickHelper(UPlayerMovement* Target, bool bTickInEditor, float DeltaTime, ELevelTick TickType, const ExecuteTickLambda& ExecuteTickFunc);	
};

template<>
struct TStructOpsTypeTraits<FPostPhysicsTickFunction> : public TStructOpsTypeTraitsBase2<FPostPhysicsTickFunction>
{
	enum
	{
		WithCopy = false
	};
};

template <typename ExecuteTickLambda>
void FPostPhysicsTickFunction::ExecuteTickHelper(UPlayerMovement* Target, bool bTickInEditor, float DeltaTime, ELevelTick TickType, const ExecuteTickLambda& ExecuteTickFunc)
{
	if (Target && !Target->IsPendingKillOrUnreachable())
	{
		FScopeCycleCounterUObject ComponentScope(Target);
		FScopeCycleCounterUObject AdditionalScope(Target->AdditionalStatObject());

		if (Target->bRegistered)
		{
			AActor* MyOwner = Target->GetOwner();
			//@optimization, I imagine this is all unnecessary in a shipping game with no editor
			if (TickType != LEVELTICK_ViewportsOnly ||
				(bTickInEditor && TickType == LEVELTICK_ViewportsOnly) ||
				(MyOwner && MyOwner->ShouldTickIfViewportsOnly())
				)
			{
				const float TimeDilation = (MyOwner ? MyOwner->CustomTimeDilation : 1.f);
				ExecuteTickFunc(DeltaTime * TimeDilation);
			}
		}
	}
}

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MIRAGE_API UPlayerMovement : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPlayerMovement();

	UPROPERTY(EditDefaultsOnly, Category="ComponentTick")
	struct FPostPhysicsTickFunction PostPhysicsTickFunction;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	virtual void RegisterComponentTickFunctions(bool bRegister) override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void TickComponentPostPhysics(float DeltaTime, ELevelTick TickType, FPostPhysicsTickFunction* ThisTickFunction);

	/** Get the movement in X axis between last frame and current frame */
	float GetMovement() const;
	/** Returns the walking boundaries represented by an FVector4 where:
	 * X = Minimum X
	 * Y = Maximum X
	 * Z = Minimum Y
	 * W = Maximum Y
	 */
	FVector4 GetBoundaries() const;
	/** Ignore collisions with ActorToIgnore */
	void IgnoreCollisionsWithActor(AActor* ActorToIgnore) const;

private:
	UPROPERTY()
	APawn* Owner = nullptr;
	/** 
	 * Represents the boundaries in which the Player can walk - Translated from StartLocation
	 * X = Minimum X (StartLocation.X + X)
	 * Y = Maximum X (StartLocation.X + Y)
	 * Z = Minimum Y (StartLocation.Y + Z)
	 * W = Maximum Y (StartLocation.Y + W)
	 */
	UPROPERTY(EditAnywhere)
	FVector4 Boundaries;
	UPROPERTY(VisibleAnywhere)
	float Movement = 0.f;
	FVector LocationLastFrame;
	FVector StartLocation;

	/** Check if position in X-axis is beyond MinMaxXLocation */
	bool ShouldRegisterMovement() const;
	/** Set the Movement variable to calculate movement speed */
	void UpdateMovement();
	/** Reset Player's X position to be within MinMaxXLocation */
	void ResetLocation();

	friend struct FPostPhysicsTickFunction;
};
